<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

use DateTimeInterface;
use Iterator;
use RuntimeException;
use Stringable;

/**
 * ApiFrInseeBanEndpointInterface class file.
 * 
 * This class represents the driver to get bulk files from the insee (which 
 * are deposed on adresse.data.gouv.fr).
 * 
 * @author Anastaszor
 */
interface ApiFrInseeBanEndpointInterface extends Stringable
{
	
	/**
	 * Gets all the dates when the export took place.
	 * 
	 * @return array<integer, DateTimeInterface>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getUploadDates() : array;
	
	/**
	 * Gets the latest available dates from the upload.
	 * 
	 * @return DateTimeInterface
	 * @throws RuntimeException
	 */
	public function getLatestDate() : DateTimeInterface;
	
	/**
	 * Gets the departement codes from the ban data for the given date.
	 * 
	 * @param DateTimeInterface $dti
	 * @return array<integer, string>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getDepartementIds(DateTimeInterface $dti) : array;
	
	/**
	 * Gets the iterator for the ban lines for the date and departement.
	 * 
	 * @param DateTimeInterface $dti
	 * @param string $departementId
	 * @return Iterator<integer, ApiFrInseeBanLineInterface>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getBanLinesIterator(DateTimeInterface $dti, string $departementId) : Iterator;
	
	/**
	 * Gets the iterator for the ban ign addresses for the date and department.
	 * 
	 * @param DateTimeInterface $dti
	 * @param string $departementId
	 * @return Iterator<integer, ApiFrInseeBanIgnAddressInterface>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getBanIgnAddressesIterator(DateTimeInterface $dti, string $departementId) : Iterator;
	
	/**
	 * Gets the iterator for the ban ign groups for the date and department.
	 * 
	 * @param DateTimeInterface $dti
	 * @param string $departementId
	 * @return Iterator<integer, ApiFrInseeBanIgnGroupInterface>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getBanIgnGroupsIterator(DateTimeInterface $dti, string $departementId) : Iterator;
	
	/**
	 * Gets all the types of localisation that are used in the ban.
	 * 
	 * @return Iterator<integer, ApiFrInseeBanLocalisationInterface>
	 */
	public function getBanLocalisationIterator() : Iterator;
	
	/**
	 * Gets all the known sources that helped construct the ban.
	 * 
	 * @return Iterator<integer, ApiFrInseeBanSourceInterface>
	 */
	public function getBanSourceIterator() : Iterator;
	
	/**
	 * Gets the iterator for the type voies.
	 *
	 * @return Iterator<integer, ApiFrInseeBanTypeVoieInterface>
	 */
	public function getTypeVoieIterator() : Iterator;
	
}
