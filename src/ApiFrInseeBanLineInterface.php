<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

use DateTimeInterface;
use Stringable;

/**
 * ApiFrInseeBanLineInterface interface file.
 * 
 * This class represents the ban lines from any of the ban files.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeBanLineInterface extends Stringable
{
	
	/**
	 * Gets identifiant unique national de la position.
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « position », suivi
	 * d'une chaîne alphanumérique de 32 caractères, le tout séparés par
	 * « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-position-566461297048461789fd46e0014c4602
	 * 
	 * @return string
	 */
	public function getIdBanPosition() : string;
	
	/**
	 * Gets identifiant unique national d'adresse.
	 * 
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « housenumber »
	 * suivi d'une chaîne alphanumérique de 32 caractères, le tout séparés
	 * par « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-housenumber-129771e2247a412dadf22a178708441a
	 * 
	 * @return string
	 */
	public function getIdBanAdresse() : string;
	
	/**
	 * Gets clé nationale d'interopérabilité.
	 * Elle définit une valeur unique de l'adresse. Elle est obtenue par
	 * concaténation :
	 *  • du code INSEE de la commune sur 5 caractères ;
	 *  • de l'identifiant fantoir de la voie auquel l'adresse est rattachée
	 * sur 4 caractères ;
	 *  • du numéro de l'adresse ;
	 *  • et, le cas échéant, de l'indice de répétition, en minuscule.
	 * Tous étant séparés par « _ ».
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : 90001_0019_13_bis
	 * 
	 * @return ?string
	 */
	public function getCleInterop() : ?string;
	
	/**
	 * Gets identifiant national du groupe (voie, lieu-dit, ...).
	 * 
	 * Il est composé d'un préfixe : « ban », puis désigne la classe
	 * d'objets de l'objet dans la BAN, c'est à dire ici « group » suivi
	 * d'une chaîne alphanumérique de 32 caractères, le tout séparés par
	 * « - ».
	 * 
	 * Obligatoire
	 * 
	 * Ex : ban-group-aef96b8b449f4163917f878c4d277867
	 * 
	 * @return string
	 */
	public function getIdBanGroup() : string;
	
	/**
	 * Gets identifiant FANTOIR (Fichier ANnuaire TOpographique Initialisé
	 * Réduit).
	 * Il s'agit de la concaténation du code INSEE de la commune et d'un code
	 * alphanumérique de 4 caractères.
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : 901050015
	 * 
	 * @return ?string
	 */
	public function getIdFantoir() : ?string;
	
	/**
	 * Gets il s'agit du numéro de l'adresse dans la voie, sans indice de
	 * répétition.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2 pour « 2 bis RUE PASTEUR »
	 * /!\ 0 (zéro) => Numérotation fictive dans le cas de voies ou lieux-dit
	 * sans numéros
	 * /!\ Adresse dite en « 5000 » ou « 9000 » => Ces numérotations sont
	 * attribuées par la DGFiP et correspondent à une numérotation
	 * provisoire
	 * 
	 * @return int
	 */
	public function getNumero() : int;
	
	/**
	 * Gets indice de répétition.
	 * 
	 * Mention qui complète une numérotation de voirie. L'indice de
	 * répétition permet de différencier plusieurs adresses portant le même
	 * numéro dans la même rue.
	 * 
	 * Non Obligatoire
	 * 
	 * Ex : BIS, TER, QUATER, QUINQUIES, … ou une lettre A, B, C, D, U, V, W
	 * … ou un chiffre 1, 2, 3
	 * 
	 * @return ?string
	 */
	public function getSuffixe() : ?string;
	
	/**
	 * Gets nom complet de la voie ou du lieu-dit de l'adresse.
	 * 
	 * Cas particulier : suite à une fusion de communes, lorsque deux voies
	 * sont homonymes sur la même commune fusionnée, le nom de l'ancienne
	 * commune est ajouté entre parenthèse à la suite du nom de la voie.
	 * Exemple : le 01/01/2016, création de la commune nouvelle de
	 * Bourgvallées en lieu et place des communes de Gourfaleur, de La
	 * Mancellière-sur-Vire, de Saint-Romphaire et de
	 * Saint-Samson-de-Bonfossé devenues déléguées.
	 * Plusieurs « rue des écoles » dans cette nouvelle commune :
	 * • RUE DES ECOLES (SAINT-ROMPHAIRE)
	 * • RUE DES ECOLES (GOURFALEUR)
	 * • RUE DES ECOLES
	 * 
	 * Obligatoire
	 * 
	 * Ex : RUE DES ECOLES (SAINT-ROMPHAIRE)
	 * 
	 * @return string
	 */
	public function getNomVoie() : string;
	
	/**
	 * Gets code postal de l'adresse, servant à la distribution du courrier.
	 * 
	 * Pour plus d'information sur la correspondance CODE INSEE / CODE POSTAL
	 * consulter le site :
	 * https://www.data.gouv.fr/fr/datasets/base-officielle-des-codes-postaux/.
	 * 
	 * Non Obligatoire
	 * 
	 * @return ?string
	 */
	public function getCodePostal() : ?string;
	
	/**
	 * Gets nom officiel de la commune (Libellé Riche).
	 * 
	 * Obligatoire.
	 * 
	 * @return string
	 */
	public function getNomCommune() : string;
	
	/**
	 * Gets numéro INSEE de la commune de l'adresse.
	 * 
	 * Une commune nouvelle résultant d'un regroupement de communes
	 * préexistantes se voit attribuer le code INSEE de l'ancienne commune
	 * désignée comme chef-lieu par l'arrêté préfectoral qui l'institue.
	 * En conséquence une commune change de code INSEE si un arrêté
	 * préfectoral modifie son chef-lieu.
	 * 
	 * Obligatoire
	 * 
	 * @return string
	 */
	public function getCodeInsee() : string;
	
	/**
	 * Gets complément du nom de la voie, utilisé en particulier pour
	 * conserver les communes déléguées en cas de fusions de communes.
	 * 
	 * Non Obligatoire
	 * 
	 * @return ?string
	 */
	public function getNomComplementaire() : ?string;
	
	/**
	 * Gets position du nom ?
	 * 
	 * @return ?string
	 */
	public function getPosName() : ?string;
	
	/**
	 * Gets coordonnée cartographique de l'abscisse exprimée en projections
	 * légales (Lambert 93).
	 * 
	 * Obligatoire
	 * 
	 * @return float
	 */
	public function getX() : float;
	
	/**
	 * Gets coordonnée cartographique de l'ordonnée exprimée en projections
	 * légales (Lambert 93).
	 * 
	 * Obligatoire
	 * 
	 * @return float
	 */
	public function getY() : float;
	
	/**
	 * Gets longitude en WGS84.
	 * 
	 * Obligatoire
	 * 
	 * @return float
	 */
	public function getLon() : float;
	
	/**
	 * Gets latitude en WGS84.
	 * 
	 * Obligatoire
	 * 
	 * @return float
	 */
	public function getLat() : float;
	
	/**
	 * Gets type de localisation de l'adresse.
	 * 
	 * Obligatoire
	 * 
	 * @return ApiFrInseeBanLocalisationInterface
	 */
	public function getTypLoc() : ApiFrInseeBanLocalisationInterface;
	
	/**
	 * Gets source de l'adresse.
	 * 
	 * dgfip                    | Direction Générale des Finances Publiques
	 * ign                      | Institut national de l'information
	 * géographique et forestière
	 * municipal_administration | Commune
	 * 
	 * Obligatoire
	 * 
	 * @return ApiFrInseeBanSourceInterface
	 */
	public function getSource() : ApiFrInseeBanSourceInterface;
	
	/**
	 * Gets date de la dernière mise à jour du groupe.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDerMajGroup() : DateTimeInterface;
	
	/**
	 * Gets date de la dernière mise à jour de l'adresse.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDerMajHn() : DateTimeInterface;
	
	/**
	 * Gets date de la dernière mise à jour de la position.
	 * 
	 * Obligatoire
	 * 
	 * Ex : 2018-10-18 23:31:48.501372+02
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDerMajPos() : DateTimeInterface;
	
}
