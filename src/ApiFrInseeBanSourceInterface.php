<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

use Stringable;

/**
 * ApiFrInseeBanSourceInterface interface file.
 * 
 * This represents the source of a given adresse localisation.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeBanSourceInterface extends Stringable
{
	
	/**
	 * Gets the id of the source.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the code of the source.
	 * 
	 * @return string
	 */
	public function getCode() : string;
	
	/**
	 * Gets the definition of the source.
	 * 
	 * @return string
	 */
	public function getDefinition() : string;
	
}
