<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

use Stringable;

/**
 * ApiFrInseeBanLocalisationInterface interface file.
 * 
 * This represents the type of localisation with its precision.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeBanLocalisationInterface extends Stringable
{
	
	/**
	 * Gets the id of the localisation.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the code of the type localisation.
	 * 
	 * @return string
	 */
	public function getCode() : string;
	
	/**
	 * Gets the precision of the type localisation (lower is better).
	 * 
	 * @return int
	 */
	public function getPrecision() : int;
	
	/**
	 * Gets the definition of the type localisation (fr).
	 * 
	 * @return string
	 */
	public function getDefinition() : string;
	
}
