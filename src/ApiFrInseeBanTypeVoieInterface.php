<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

use Stringable;

/**
 * ApiFrInseeBanTypeVoieInterface interface file.
 * 
 * This represents the type of way for an address.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeBanTypeVoieInterface extends Stringable
{
	
	/**
	 * Gets the id of the type voie.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the code of the type voie.
	 * 
	 * @return string
	 */
	public function getCode() : string;
	
	/**
	 * Gets the full name of the type voie.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
}
