<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-ban-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeBan;

use Stringable;

/**
 * ApiFrInseeBanIgnAddressInterface interface file.
 * 
 * This class represents the joint table between the BAN Address and IGN
 * Address.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeBanIgnAddressInterface extends Stringable
{
	
	/**
	 * Gets the ban address id.
	 * 
	 * @return string
	 */
	public function getIdBanAdresse() : string;
	
	/**
	 * Gets the ign address id.
	 * 
	 * @return string
	 */
	public function getIdIgnAdresse() : string;
	
}
